import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
} from "recharts";

export default function ChartJS({ datasets, total }) {
  return (
    <BarChart
      width={500}
      height={300}
      data={datasets}
      margin={{
        top: 15,
        right: 40,
        left: 30,
        bottom: 30,
      }}
    >
      <XAxis dataKey="name" offset={20}>
        <Label value="Total Task" position="left" offset={10} />
      </XAxis>
      <YAxis type="number" domain={[0, total]} />
      <Tooltip />
      <CartesianGrid strokeDasharray="3 3" />
      <Bar dataKey="numberTask" fill="#8884d8" offset={20} />
    </BarChart>
  );
}
