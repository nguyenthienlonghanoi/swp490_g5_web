/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useCallback, useEffect, useContext } from "react";
import FlexBetween from "components/FlexBetween";
import Header from "components/Header";
import { Search } from "@mui/icons-material";
import {
  Box,
  Backdrop,
  IconButton,
  useTheme,
  useMediaQuery,
  InputBase,
  CircularProgress,
  Grid,
  Typography,
} from "@mui/material";
import { AlertContext } from "../../components/AlertProvider";
// table
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import ChartJS from "components/BarChart";
import {
  getAllProject,
  getProjectByLead,
  getTaskByProjectId,
} from "store/projectStore";
import { getUserActiveStatistic } from "store/userStore";
import { useDispatch, useSelector } from "react-redux";

import moment from "moment";
import {
  defaultPagination,
  getStatusName,
  getRowNumber,
  globalConstant,
} from "services/util";
import projectService from "services/project";
import { getUserByProject } from "store/userStore";
import { DataGrid } from "@mui/x-data-grid";
import dayjs from "dayjs";

function Rows(props) {
  const { rows, rowSelected } = props;
  const [isActiveRow, setIsActiveRow] = useState(false);
  return rows.map((data) => {
    let row = data;
    return (
      <TableRow
        key={row.no}
        onClick={() => {
          setIsActiveRow(row.projectId);
          rowSelected(row);
        }}
        hover
        sx={{
          "& > *": { borderBottom: "unset" },
          "&:last-child td,&:last-child th": { border: 0 },
          background:
            isActiveRow === row.projectId ? "rgba(0, 0, 0, 0.04);" : " ",
        }}
      >
        <TableCell component="th" scope="row">
          {row.no}
        </TableCell>
        <TableCell>{row.projectName}</TableCell>
        <TableCell>{row.description}</TableCell>
        <TableCell>
          {moment(row.createDate).format(globalConstant.dateFormat)}
        </TableCell>
        <TableCell>{moment(row.deadLine).format(globalConstant.dateFormat)}</TableCell>
      </TableRow>
    );
  });
}

function Rows2(props) {
  const { rows } = props;
  return rows.map((data) => {
    let row = data;
    return (
      <TableRow
        key={row.code}
        hover
        sx={{
          "& > *": { borderBottom: "unset" },
          "&:last-child td,&:last-child th": { border: 0 },
        }}
      >
        <TableCell component="th" scope="row"></TableCell>
        <TableCell>{row.code}</TableCell>
        <TableCell>{row.summary}</TableCell>
        <TableCell>
          {row.priorityId === 1 && (
            <Typography style={{ color: "red" }}>EXTREME</Typography>
          )}
          {row.priorityId === 2 && (
            <Typography style={{ color: "orange" }}>HIGH</Typography>
          )}
          {row.priorityId === 3 && (
            <Typography style={{ color: "yellow" }}>MODERATE</Typography>
          )}
          {row.priorityId === 4 && (
            <Typography style={{ color: "blue" }}>LOW</Typography>
          )}
        </TableCell>
      </TableRow>
    );
  });
}

function Rows3(props) {
  const { rows } = props;
  return rows.map((data) => {
    let row = data;
    return (
      <TableRow
        key={row.userId}
        hover
        sx={{
          "& > *": { borderBottom: "unset" },
          "&:last-child td,&:last-child th": { border: 0 },
        }}
      >
        <TableCell>{row.no}</TableCell>
        <TableCell>{row.fullName}</TableCell>
        <TableCell>{row.username}</TableCell>
        <TableCell>{row.emailAddress}</TableCell>
      </TableRow>
    );
  });
}

const Dashboard = () => {
  const theme = useTheme();
  const isNonMediumScreens = useMediaQuery("(min-width: 1200px)");
  const [searchText, setSearchText] = useState("");
  const [searchText1, setSearchText1] = useState("");
  const { actions } = useContext(AlertContext);

  const [projectList, setProjectList] = useState([]);
  const [totalItems, setTotalItems] = useState(0);
  const [pagination, setPagination] = useState(defaultPagination);
  const [pagination2, setPagination2] = useState(defaultPagination);
  const role = localStorage.getItem("role");
  const [loading, setLoading] = useState(false);
  const [labels, setLabels] = useState([]);
  const [datasets, setdatasets] = useState();
  const [listTask, setListTask] = useState([]);
  const dispatch = useDispatch();
  const [userByProjectList, setUserByProjectList] = useState([]);
  const [isloading, setIsLoading] = useState(false);

  const [userLoading, setUserLoading] = useState(false);
  const [userList, setUserList] = useState([]);
  const filterModel = {
    items: [
      {
        columnField: "fullName",
        operatorValue: "contains",
        value: searchText1,
      },
    ],
  };

  const initFetch = useCallback(async () => {
    setUserLoading(true);
    dispatch(getUserActiveStatistic())
      .then((response) => {
        if (response.payload) {
          const data = response.payload.map((item, index) => {
            return { no: index + 1, ...item };
          });
          setUserList(data);
        }
      })
      .catch((e) => {})
      .finally(() => setUserLoading(false));
  }, [dispatch]);

  const getProjectList = useCallback(
    (pagination) => {
      const params = {
        limit: pagination.pageSize,
        page: pagination.currentPage,
        status: 3,
      };
      setLoading(true);
      dispatch(
        role === "ADMIN" ? getAllProject(params) : getProjectByLead(params)
      )
        .then((response) => {
          if (response.payload) {
            if (response.payload.data.length === 0) {
              actions.addAlert({
                text: "There is no active project",
                type: "info",
                id: Date.now(),
              });
            } else {
              const data = response.payload.data.map((item, index) => {
                return {
                  no: getRowNumber(
                    pagination.currentPage,
                    pagination.pageSize,
                    index
                  ),
                  ...item,
                };
              });
              setProjectList(data);
            }
          }
        })
        .catch((e) => {})
        .finally(() => setLoading(false));
    },
    [dispatch]
  );

  useEffect(() => {
    if (role === "PM") {
      getProjectList(pagination);
    } else initFetch();
  }, [getProjectList, pagination]);

  const filteredData = projectList.filter((item) =>
    item.projectName.toLowerCase().includes(searchText.toLowerCase())
  );

  // const handleChangePage = (event, newPage) => {
  //   setPagination((prevState) => ({
  //     ...prevState,
  //     currentPage: newPage + 1,
  //   }));
  // };

  // const handleChangeRowsPerPage = (event) => {
  //   setPagination((prevState) => ({
  //     ...prevState,
  //     pageSize: event.target.value,
  //   }));
  // };

  const Columns = [
    {
      field: "no",
      headerName: "No",
      key: "id",
      flex: 0.1,
    },
    {
      field: "fullName",
      headerName: "Full Name",
      key: "fullName",
      flex: 1,
    },
    {
      field: "username",
      headerName: "User Name",
      key: "username",
      flex: 0.8,
    },
    {
      field: "emailAddress",
      headerName: "Email",
      key: "emailAddress",
      flex: 1,
    },
    {
      field: "phone",
      headerName: "Phone",
      key: "phone",
      flex: 1,
    },
    {
      field: "jobTitle",
      headerName: "Job Title",
      key: "jobTitle",
      flex: 1,
    },
    {
      field: "missTime",
      headerName: "Miss Time",
      key: "missTime",
      flex: 0.6,
    },
    {
      field: "ontime",
      headerName: "On Time",
      key: "ontime",
      flex: 0.5,
    },
    {
      field: "numberTaskIsMain",
      headerName: "Task as Main",
      key: "numberTaskIsMain",
      flex: 0.8,
    },
    {
      field: "numberTaskIsMember",
      headerName: "Task as Member",
      key: "numberTaskIsMember",
      flex: 1,
    },
  ];

  const getUserByProjectId = useCallback(
    (pagination, projectId) => {
      const params = {
        limit: pagination.pageSize,
        page: pagination.currentPage,
        projectId: projectId,
      };
      setIsLoading(true);
      dispatch(getUserByProject(params))
        .then((response) => {
          if (response.payload) {
            const data = response.payload.data.map((item, index) => {
              return {
                no: getRowNumber(
                  pagination.currentPage,
                  pagination.pageSize,
                  index
                ),
                ...item,
              };
            });
            setUserByProjectList(data);
          }
        })
        .catch((e) => {})
        .finally(() => setIsLoading(false));
    },
    [dispatch]
  );

  const rowSelected = async (row) => {
    setLoading(true);
    let { data } = await projectService.getStatisticTask(row.projectId);
    const task = data.chartTasks;
    setTotalItems(data.totalTask);
    setdatasets(task);
    projectService.getTaskByProjectId(row.projectId).then((data) => {
      const sortedData = data.data.sort((a, b) => b.taskId - a.taskId);
      setListTask(sortedData);
      setLoading(false);
    });
    getUserByProjectId(pagination2, row.projectId);
  };

  return (
    <Box>
      {role === "PM" ? (
        <Box sx={{ width: "auto" }} m="0.5rem 2.5rem">
          <FlexBetween>
            <Box>
              <Header title="DASHBOARD" subtitle="Welcome to your dashboard" />
            </Box>
          </FlexBetween>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            sx={{ height: "100%" }}
          >
            <Grid item xs={7}>
              <FlexBetween>
                <Box>
                  <FlexBetween
                    backgroundColor={theme.palette.background.alt}
                    borderRadius="9px"
                    gap="3rem"
                    p="0.1rem 1.5rem"
                    sx={{
                      mt: "20px",
                    }}
                  >
                    <InputBase
                      placeholder="Search By Project Name"
                      value={searchText}
                      onChange={(e) => setSearchText(e.target.value)}
                    />
                    <IconButton>
                      <Search />
                    </IconButton>
                  </FlexBetween>
                </Box>
              </FlexBetween>
              <Box
                mt="20px"
                display="grid"
                sx={{
                  "& > div": {
                    gridColumn: isNonMediumScreens ? undefined : "span 12",
                  },
                }}
              >
                <Box
                  sx={{
                    "& .MuiPaper-root": {
                      border: "none",
                      borderRadius: ".5rem",
                      height: "33vh",
                      backgroundColor: theme.palette.background.alt,
                    },
                    "& .MuiTableCell-root": {
                      fontSize: "14px",
                      "&:hover": {
                        cursor: "pointer",
                      },
                    },
                    "& .MuiTableCell-head": {
                      backgroundColor: theme.palette.background.alt,
                      color: theme.palette.secondary[100],
                    },
                    "& .MuiPaper-virtualScroller": {
                      backgroundColor: "#cca752",
                    },
                    "& .MuiPaper-footerContainer": {
                      backgroundColor: theme.palette.background.alt,
                      color: theme.palette.secondary[100],
                      borderTop: "none",
                    },
                    "& .MuiPaper-toolbarContainer .MuiButton-text": {
                      color: `${theme.palette.secondary[200]} !important`,
                    },
                  }}
                >
                  <Typography
                    style={{ textAlign: "center", background: "#f0f0f0" }}
                    variant="h5"
                    gutterBottom
                  >
                    ACTIVE PROJECT OF COMPANY
                  </Typography>
                  <TableContainer component={Paper}>
                    <Table>
                      <TableHead
                        sx={{
                          position: "sticky",
                          top: 0,
                          zIndex: 1,
                        }}
                      >
                        <TableRow>
                          <TableCell>No</TableCell>
                          <TableCell>Project</TableCell>
                          <TableCell>Description</TableCell>
                          <TableCell>Created At</TableCell>
                          <TableCell>DeadLine</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        <Rows rows={filteredData} rowSelected={rowSelected} />
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <Backdrop
                    sx={{
                      color: "#fff",
                      zIndex: (theme) => theme.zIndex.drawer + 1,
                    }}
                    open={loading}
                  >
                    <CircularProgress color="inherit" />
                  </Backdrop>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={5}>
              <Box
                sx={{
                  paddingTop: "79px",
                  "& .MuiPaper-root": {
                    height: "33vh",
                    backgroundColor: "#90AFC5",
                  },
                  "& .MuiTableCell-root": {
                    fontSize: "14px",
                    "&:hover": {
                      cursor: "pointer",
                    },
                    backgroundColor: "#90AFC5",
                    color: "white",
                  },
                  "& .MuiTableCell-head": {
                    backgroundColor: "#90AFC5",
                    color: "#fff",
                    fontSize: "14px",
                  },
                  "& .MuiPaper-virtualScroller": {
                    backgroundColor: "#90AFC5",
                  },
                  "& .MuiPaper-footerContainer": {
                    backgroundColor: "#90AFC5",
                    color: theme.palette.secondary[100],
                  },
                  "& .MuiPaper-toolbarContainer .MuiButton-text": {
                    color: `${theme.palette.secondary[200]} !important`,
                  },
                }}
              >
                <Typography
                  style={{
                    textAlign: "center",
                    background: "#90AFC5",
                    color: "#fff",
                  }}
                  variant="h5"
                  gutterBottom
                >
                  LIST TASK OF PROJECT
                </Typography>
                <TableContainer component={Paper}>
                  <Table
                    aria-label="collapsible table"
                    sx={{
                      background: theme.palette.background.alt,
                    }}
                  >
                    <TableHead
                      sx={{
                        position: "sticky",
                        top: 0,
                        zIndex: 1,
                      }}
                    >
                      <TableRow>
                        <TableCell />
                        <TableCell>Code</TableCell>
                        <TableCell>Summary</TableCell>
                        <TableCell>Priority</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <Rows2 rows={listTask} />
                    </TableBody>
                  </Table>
                </TableContainer>
                <Backdrop
                  sx={{
                    color: "#fff",
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                  }}
                  open={loading}
                >
                  <CircularProgress color="inherit" />
                </Backdrop>
              </Box>
            </Grid>
            <Grid item xs={7}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                  margin: 1,
                  marginTop: 5,
                }}
              >
                <Typography
                  style={{ marginLeft: "35px", marginBottom: "10px" }}
                  variant="h5"
                >
                  Total task by workflow step
                </Typography>
                <ChartJS datasets={datasets} total={totalItems} />
              </Box>
            </Grid>

            <Grid item xs={5}>
              <Box
                sx={{
                  marginTop: "30px",
                  "& .MuiPaper-root": {
                    height: "33vh",
                    backgroundColor: "#336B87",
                  },
                  "& .MuiTableCell-root": {
                    fontSize: "14px",
                    "&:hover": {
                      cursor: "pointer",
                    },
                    backgroundColor: "#336B87",
                    color: "white",
                  },
                  "& .MuiTableCell-head": {
                    backgroundColor: "#336B87",
                    color: "#fff",
                    fontSize: "14px",
                  },
                  "& .MuiPaper-virtualScroller": {
                    backgroundColor: "#cca752",
                  },
                  "& .MuiPaper-footerContainer": {
                    backgroundColor: "#336B87",
                    color: theme.palette.secondary[100],
                  },
                  "& .MuiPaper-toolbarContainer .MuiButton-text": {
                    color: `${theme.palette.secondary[200]} !important`,
                  },
                }}
              >
                <Typography
                  style={{
                    textAlign: "center",
                    background: "#336B87",
                    color: "#fff",
                  }}
                  variant="h5"
                  gutterBottom
                >
                  MEMBER OF PROJECT
                </Typography>
                <TableContainer component={Paper}>
                  <Table
                    sx={{
                      background: "#336B87",
                    }}
                  >
                    <TableHead
                      sx={{
                        position: "sticky",
                        top: 0,
                        zIndex: 1,
                      }}
                    >
                      <TableRow>
                        <TableCell>No</TableCell>
                        <TableCell>Full Name</TableCell>
                        <TableCell>User Name</TableCell>
                        <TableCell>Email</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <Rows3 rows={userByProjectList} />
                    </TableBody>
                  </Table>
                </TableContainer>
              </Box>
            </Grid>
          </Grid>
        </Box>
      ) : (
        <Box m="1.5rem 2.5rem">
          <Header title="DASHBOARD" subtitle="Welcome to your dashboard" />
          <FlexBetween
            backgroundColor={theme.palette.background.alt}
            borderRadius="9px"
            gap="3rem"
            p="0.1rem 1.5rem"
            sx={{
              mt: "20px",
              width: "319.562px",
            }}
          >
            <InputBase
              placeholder="Search by full name..."
              value={searchText1}
              onChange={(e) => setSearchText1(e.target.value)}
            />
            <IconButton>
              <Search />
            </IconButton>
          </FlexBetween>
          <Box
            mt="40px"
            height="65vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
                fontSize: "14px",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .MuiDataGrid-cell:hover": {
                cursor: "pointer",
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: theme.palette.background.alt,
                color: theme.palette.secondary[100],
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: theme.palette.primary.light,
              },
              "& .MuiDataGrid-footerContainer": {
                backgroundColor: theme.palette.background.alt,
                color: theme.palette.secondary[100],
                borderTop: "none",
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                color: `${theme.palette.secondary[200]} !important`,
              },
            }}
          >
            <DataGrid
              loading={userLoading}
              getRowId={(rows) => rows.userId}
              rows={userList}
              columns={Columns}
              filterModel={filterModel}
              pageSize={20}
              rowsPerPageOptions={[20]}
            />
          </Box>
          <Backdrop
            sx={{
              color: "#fff",
              zIndex: (theme) => theme.zIndex.drawer + 1,
            }}
            open={userLoading}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </Box>
      )}
    </Box>
  );
};

export default Dashboard;
